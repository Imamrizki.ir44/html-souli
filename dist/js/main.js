const passwordToggle = document.querySelector('.js-password-toggle')
const passwordToggle2 = document.querySelector('.js-confirm-password-toggle')

passwordToggle.addEventListener('change', function() {
  const password = document.querySelector('.js-password'),
    passwordLabel = document.querySelector('.js-password-label')

  if (password.type === 'password') {
    password.type = 'text'
    passwordLabel.innerHTML = '<i class="fa fa-eye-slash fa-lg" aria-hidden="true" id="eye"></i>'
  } else {
    password.type = 'password'
    passwordLabel.innerHTML = '<i class="fa fa-eye fa-lg" aria-hidden="true" id="eye"></i>'
  }

  password.focus()
})

passwordToggle2.addEventListener('change', function() {
  const password2 = document.querySelector('.js-confirm-password'),
    passwordLabel2 = document.querySelector('.js-confirm-password-label')

  if (password2.type === 'password') {
    password2.type = 'text'
    passwordLabel2.innerHTML = '<i class="fa fa-eye-slash fa-lg" aria-hidden="true" id="eye2"></i>'
  } else {
    password2.type = 'password'
    passwordLabel2.innerHTML = '<i class="fa fa-eye fa-lg" aria-hidden="true" id="eye2"></i>'
  }

  password2.focus()
})