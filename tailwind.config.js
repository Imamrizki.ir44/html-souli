/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  theme: {
    extend: {
      backgroundImage: {
        'pattern': ('url(/dist/img/assets/Frame_169.png)'),
        'pattern-white': ('url(/dist/img/assets/patern-white.png)'),
      },
      colors: {
        'grey-souli': '#d1d5db' 
      }
    },
  },
  daisyui: {
    themes: [
      {
        light: {
        "primary": "#6937AE",
        "secondary": "#CDBCE4",
        "accent": "#462574",
        "neutral": "#FAF7FF",
        "base-100": "#CDBCE4",
        "info": "#3ABFF8",
        "success": "#36D399",
        "warning": "#FBBD23",
        "error": "#F87272",
        'link': "#EF2EAD"
        },
      },
    ],
  },
  plugins: [require("daisyui")],
}
